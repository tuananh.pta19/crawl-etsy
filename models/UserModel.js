let mongoose = require('../config/DBConfig');

const UserSchema = new mongoose.Schema({
    feedback_id: Number,
    creation_tsz: Number,
    message: String,
    value: Number,
    buyer_user_id: Number,
    creator_user_id: Number,
    target_user_id: Number
});
const UserModel = mongoose.model('users', UserSchema);

module.exports = UserModel;

