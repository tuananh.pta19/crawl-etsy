let mongoose = require('../config/DBConfig');

const ShopSchema = new mongoose.Schema({
    receipt_id: Number,
    date: Number,
    buyer_user_id: Number,
    buyer_real_name: String,
    buyer_login_name: String,
    buyer_avatar_url: String,
    buyer_avatar_image_key: Object,
    buyer_is_anonymous: Boolean,
    buyer_real_name_or_login_name: String,
    buyer_casual_name_or_login_name: String,
    buyer_is_active: Boolean,
    buyer_is_guest: Boolean,
    buyer_is_name_withheld: Boolean,
    reviews: [{
        transaction_id: Number,
        listing_id: Number,
        listing_title: String,
        listing_image_url: String,
        listing_image: Object,
        review: String,
        rating: Number,
        is_response_deleted: Boolean,
        language: String,
        listing: Object,
        seller_left_feedback: Boolean,
        buyer_left_feedback: Boolean,
        is_listing_displayable: Boolean,
        update_date: Number
    }]
});
const ShopModel = mongoose.model('shop3', ShopSchema);
module.exports = ShopModel;

